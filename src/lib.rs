extern crate byteorder;
extern crate flate2;
extern crate bzip2;

use std::fs::File;
use std::io::Write;
use std::io;
use std::io::{BufReader, Read};
use std::io::{Cursor, Seek, SeekFrom};

use byteorder::{LittleEndian, ReadBytesExt};
use flate2::write::ZlibDecoder;
use bzip2::write::BzDecoder;

mod header;
mod tables;
mod hasher;

use tables::MpqTableEntriesRead;

#[cfg(test)]
mod tests;

enum MpqFlags {
    #[allow(dead_code)]
    MpqFileImplode       = 0x00000100,
    MpqFileCompress      = 0x00000200,
    MpqFileEncrypted     = 0x00010000,
    #[allow(dead_code)]
    MpqFileFixKey        = 0x00020000,
    MpqFileSingleUnit    = 0x01000000,
    #[allow(dead_code)]
    MpqFileDeleteMarker  = 0x02000000,
    MpqFileSectorCrc     = 0x04000000,
    MpqFileExists        = 0x80000000,
}

#[derive(Debug)]
pub struct MpqArchive {
    pub file: BufReader<File>,
    pub hasher: hasher::Hasher,
    pub mpq_header: header::MpqHeader,
    pub hash_table: tables::MpqHashTable,
    pub block_table: tables::MpqBlockTable,
}

impl MpqArchive {
    pub fn open(filename: &str) -> io::Result<MpqArchive> {
        let hasher = hasher::Hasher::new();
        let file = File::open(filename).unwrap(); // TODO: error handling
        let mut reader = BufReader::new(file);
        let header = header::MpqHeader::read(&mut reader)?;

        let hash_table = {
            let hash_offset = (header.offset + header.hash_table_offset) as u64;
            reader.seek(SeekFrom::Start(hash_offset))?;
            let mut hash_buffer = vec![0u8; (header.hash_table_entries * 16) as usize];
            reader.read_exact(&mut hash_buffer)?;
            let key = hasher.hash("(hash table)", hasher::HashType::Table);
            let hash_decrypted = hasher.decrypt(&hash_buffer, key);
            let mut bufcursor = Cursor::new(hash_decrypted);
            tables::MpqHashTable::read(&mut bufcursor, SeekFrom::Start(0), header.hash_table_entries)?
        };
        let block_table = {
            let offset = (header.offset + header.block_table_offset) as u64;
            reader.seek(SeekFrom::Start(offset))?;
            let mut buffer = vec![0u8; (header.block_table_entries * 16) as usize];
            reader.read_exact(&mut buffer)?;
            let key = hasher.hash("(block table)", hasher::HashType::Table);
            let decrypted = hasher.decrypt(&buffer, key);
            let mut bufcursor = Cursor::new(decrypted);
            tables::MpqBlockTable::read(&mut bufcursor, SeekFrom::Start(0), header.block_table_entries)?
        };
        Result::Ok(MpqArchive{
            file: reader,
            hasher: hasher,
            mpq_header: header,
            hash_table: hash_table,
            block_table: block_table,
        })
    }

    pub fn read_user_header_content(&self) -> Option<Vec<u8>> {
        let h = &self.mpq_header.user_data_header;
        h.as_ref().map(|uhr| uhr.content.clone())
    }

    pub fn read_file(&mut self, filename: &str) -> io::Result<Vec<u8>> {
        let hash_entry = self.hash_table.get_entry(filename, &self.hasher);
        let block_entry = &self.block_table.entries[hash_entry.block_table_index as usize];

        if (block_entry.flags & MpqFlags::MpqFileExists as u32) != 0 {
            self.file.seek(SeekFrom::Start((block_entry.offset + self.mpq_header.offset) as u64))?;
            let mut archived_data = vec![0u8; block_entry.archived_size as usize];
            self.file.read_exact(&mut archived_data)?;
            if (block_entry.flags & MpqFlags::MpqFileEncrypted as u32) != 0 {
                unimplemented!()
            }
            if (block_entry.flags & MpqFlags::MpqFileSingleUnit as u32) != 0 {
                if (block_entry.flags & MpqFlags::MpqFileCompress as u32) != 0 && block_entry.size > block_entry.archived_size {
                    return decompress(&archived_data);
                } else {
                    unimplemented!()
                }
            } else {
                let sector_size = 512 << self.mpq_header.sector_size_shift;
                let mut sectors_count = block_entry.size / sector_size + 1;
                let has_crc = if (block_entry.flags & MpqFlags::MpqFileSectorCrc as u32) != 0 {
                    sectors_count += 1;
                    true
                } else {
                    false
                };
                let mut cursor = Cursor::new(archived_data.clone());
                let positions:Vec<u32> = (0..sectors_count).map(|_| cursor.read_u32::<LittleEndian>().unwrap()).collect();
                let mut result: Vec<u8> = Vec::new();
                for i in 0..positions.len() - if has_crc { 2 } else { 1 } {
                    let sector = &archived_data[positions[i] as usize..positions[i - 1] as usize];
                    if (block_entry.flags & MpqFlags::MpqFileCompress as u32) != 0 && block_entry.size > block_entry.archived_size {
                        result.write(&decompress(sector)?)?;
                    } else {
                        result.write(sector)?;
                    }
                }
                return Result::Ok(result);
            }
        }
        unreachable!()
    }

    pub fn files(&mut self) -> Vec<String> {
        let file = self.read_file("(listfile)").unwrap();
        let raw_str = String::from_utf8(file).unwrap();
        raw_str.split_whitespace().map(|filename| filename.to_string()).collect()
    }
}

fn decompress(data: &[u8]) -> io::Result<Vec<u8>> {
    match data[0] {
        0 => Result::Ok(Vec::from(data)),
        2 => {
            let mut writer = Vec::new();
            let mut z = ZlibDecoder::new(writer);
            z.write(&data[1..])?;
            writer = z.finish()?;
            Result::Ok(writer)
        }
        16 => {
            let mut writer = Vec::new();
            let mut z = BzDecoder::new(writer);
            z.write(&data[1..])?;
            writer = z.finish()?;
            Result::Ok(writer)
        }
        _ => unimplemented!("Decompression isn't supported yet")
    }
}