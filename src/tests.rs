use super::MpqArchive;
use std;
use std::io::Write;

#[test]
fn test_read() {
    let mut arc = MpqArchive::open("/Users/liraim/own-projects/rust-mpq/test.StormReplay").unwrap();
    let files = arc.files();
    writeln!(std::io::stdout(), "{:?}", files).unwrap();
    let file = arc.read_file("replay.details");
    writeln!(std::io::stdout(), "{:?}", file).unwrap()
    ;
}