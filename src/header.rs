use byteorder::{LittleEndian, ReadBytesExt};

use std;
use std::io;
use std::io::{BufReader, Seek, SeekFrom, Read};
use std::fs::File;

#[derive(Debug)]
pub struct MpqHeader {
    pub magic: String,
    pub header_size: u32,
    pub archive_size: u32,
    pub format_version: u16,
    pub sector_size_shift: u16,
    pub hash_table_offset: u32,
    pub block_table_offset: u32,
    pub hash_table_entries: u32,
    pub block_table_entries: u32,
    pub offset: u32,
    pub user_data_header: Option<MpqUserDataHeader>,
}

#[derive(Debug)]
pub struct MpqUserDataHeader {
    pub magic: String,
    pub user_data_size: u32,
    pub mpq_header_offset: u32,
    pub user_data_header_size: u32,
    pub content: Vec<u8>
}

impl MpqHeader {
    pub fn read(reader: &mut BufReader<File>) -> io::Result<MpqHeader> {
        let mut buffer: [u8; 4] = [0; 4];
        reader.read_exact(&mut buffer)?;
        let magic = std::str::from_utf8(&buffer).unwrap().to_string().clone();
        match magic.as_ref() {
            "MPQ\u{1b}" => {
                let mpq_user_data_header = read_mpq_user_data_header(reader, Option::Some(0))?;
                let mpq_header = read_mpq_header(
                                        reader, 
                                        Option::Some(mpq_user_data_header.mpq_header_offset as u64),
                                        Option::Some(mpq_user_data_header)
                                    )?;
                Result::Ok(mpq_header)
            },
            "MPQ\u{1a}" => {
                let mpq_header = read_mpq_header(reader, Option::Some(0), Option::None)?;
                Result::Ok(mpq_header)
            },
            _ => panic!(),
        }
    }
}

fn read_mpq_user_data_header(reader: &mut BufReader<File>, offset: Option<u64>) -> io::Result<MpqUserDataHeader> {
    if let Some(_offset) = offset {
        reader.seek(SeekFrom::Start(_offset))?;
    }
    let mut buffer: [u8; 4] = [0; 4];
    reader.read_exact(&mut buffer)?;
    let magic = std::str::from_utf8(&buffer).unwrap().to_string().clone();
    let user_data_size = reader.read_u32::<LittleEndian>().unwrap();
    let mpq_header_offset = reader.read_u32::<LittleEndian>().unwrap();
    let user_data_header_size = reader.read_u32::<LittleEndian>().unwrap();
    Result::Ok(MpqUserDataHeader{
        magic: magic,
        user_data_size: user_data_size,
        mpq_header_offset: mpq_header_offset,
        user_data_header_size: user_data_header_size,
        content: read_content(reader, user_data_header_size)?
    })
}

fn read_content(reader: &mut BufReader<File>, size: u32) -> io::Result<Vec<u8>> {
    let mut buffer: Vec<u8> = vec![0u8; size as usize];
    reader.read_exact(&mut buffer)?;
    Result::Ok(buffer)
}

fn read_mpq_header(reader: &mut BufReader<File>, offset: Option<u64>, mpq_user_data_header: Option<MpqUserDataHeader>) -> io::Result<MpqHeader> {
    if let Some(_offset) = offset {
        reader.seek(SeekFrom::Start(_offset))?;
    }
    let mut buffer: [u8; 4] = [0; 4];
    reader.read_exact(&mut buffer)?;
    let magic = std::str::from_utf8(&buffer).unwrap().to_string().clone();
    let header_size: u32 = reader.read_u32::<LittleEndian>().unwrap();
    let archive_size: u32 = reader.read_u32::<LittleEndian>().unwrap();
    let format_version: u16 = reader.read_u16::<LittleEndian>().unwrap();
    let sector_size_shift: u16 = reader.read_u16::<LittleEndian>().unwrap();
    let hash_table_offset: u32 = reader.read_u32::<LittleEndian>().unwrap();
    let block_table_offset: u32 = reader.read_u32::<LittleEndian>().unwrap();
    let hash_table_entries: u32 = reader.read_u32::<LittleEndian>().unwrap();
    let block_table_entries: u32 = reader.read_u32::<LittleEndian>().unwrap();
    Result::Ok(MpqHeader{
        magic: magic,
        header_size: header_size,
        archive_size: archive_size,
        format_version: format_version,
        sector_size_shift: sector_size_shift,
        hash_table_offset: hash_table_offset,
        block_table_offset: block_table_offset,
        hash_table_entries: hash_table_entries,
        block_table_entries: block_table_entries,
        offset: offset.unwrap_or(0) as u32,
        user_data_header: mpq_user_data_header
    })
}