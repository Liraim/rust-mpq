use byteorder::{LittleEndian, ReadBytesExt};
use std::io;
use std::io::{Read, Seek, SeekFrom};

use super::hasher::{HashType, Hasher};

pub trait Entry: Sized {
    fn read<B: ReadBytesExt + Read + Seek>(reader: &mut B, offset: SeekFrom) -> io::Result<Self>;
}

#[derive(Debug)]
pub struct MpqHashTableEntry {
    hash_a: u32,
    hash_b: u32,
    locale: u16,
    platform: u16,
    pub block_table_index: u32,
}


impl Entry for MpqHashTableEntry {
    fn read<B: ReadBytesExt + Read + Seek>(reader: &mut B, offset: SeekFrom) -> io::Result<Self> {
        reader.seek(offset)?;
        Result::Ok(Self{
            hash_a: reader.read_u32::<LittleEndian>().unwrap(),
            hash_b: reader.read_u32::<LittleEndian>().unwrap(),
            locale: reader.read_u16::<LittleEndian>().unwrap(),
            platform: reader.read_u16::<LittleEndian>().unwrap(),
            block_table_index: reader.read_u32::<LittleEndian>().unwrap(),
        })
    }
}

#[derive(Debug)]
pub struct MpqBlockTableEntry {
    pub offset: u32,
    pub archived_size: u32,
    pub size: u32,
    pub flags: u32,
}

impl Entry for MpqBlockTableEntry {
    fn read<B: ReadBytesExt + Read + Seek>(reader: &mut B, offset: SeekFrom) -> io::Result<MpqBlockTableEntry> {
        reader.seek(offset)?;
        Result::Ok(MpqBlockTableEntry {
            offset: reader.read_u32::<LittleEndian>().unwrap(),
            archived_size: reader.read_u32::<LittleEndian>().unwrap(),
            size: reader.read_u32::<LittleEndian>().unwrap(),
            flags: reader.read_u32::<LittleEndian>().unwrap(),
        })
    }
}

pub trait MpqTableEntriesRead<T: Entry> : Sized {
    fn read<B: ReadBytesExt + Read + Seek>(reader: &mut B, offset: SeekFrom, count: u32) -> io::Result<Self> {
        reader.seek(offset)?;
        let result: Result<Vec<T>, _> = (0..count).map(|x| T::read(reader, SeekFrom::Start((x * 16) as u64))).collect();
        Result::Ok(Self::create_from_vec(result?))
    }

    fn create_from_vec(Vec<T>) -> Self;
}

#[derive(Debug)]
pub struct MpqHashTable {
    pub entries: Vec<MpqHashTableEntry>,
}

impl MpqTableEntriesRead<MpqHashTableEntry> for MpqHashTable {
    fn create_from_vec(data: Vec<MpqHashTableEntry>) -> Self {
        Self{entries: data}
    }
}

#[derive(Debug)]
pub struct MpqBlockTable {
    pub entries: Vec<MpqBlockTableEntry>,
}

impl MpqTableEntriesRead<MpqBlockTableEntry> for MpqBlockTable {
    fn create_from_vec(data: Vec<MpqBlockTableEntry>) -> Self {
        Self{entries: data}
    }
}

impl MpqHashTable {
    pub fn get_entry(&self, filename: &str, hasher: &Hasher) -> &MpqHashTableEntry {
        let hash_a = hasher.hash(filename, HashType::HashA);
        let hash_b = hasher.hash(filename, HashType::HashB);

        for e in &self.entries {
            if e.hash_a == hash_a && e.hash_b == hash_b {
                return &e;
            }
        }
        unreachable!()
    }
}